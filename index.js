import express from 'express';
import mysql from 'mysql2';
import bodyParser from 'body-parser';
import { fileURLToPath } from 'url';
import { dirname } from 'path';
import {auth} from './admin.js'
import session from 'express-session';
import crypto from 'crypto';
import fileUpload from 'express-fileupload';
import fs from 'fs';



const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const timeCollection = {
    "1": '11:00',
    "2": '12:00',
    "3": '13:00',
    "4": '14:00',
    "5": '15:00',
    "6": '16:00'
}

const app = express();
const PORT = process.env.PORT ?? 80;
const urlencodedParser = bodyParser.urlencoded({ extended: false });

const pool = mysql.createPool({
    port: 3306,
    connectionLimit: 5,
    host: "localhost",
    user: "root",
    database: "barbershop",
    password: "123456"
});

app.use(session({
	secret: '13113r1uiruib93',
	resave: true,
	saveUninitialized: true
}));
app.use(fileUpload({}));
app.use(express.static(__dirname + "/public"));

app.set('view engine','hbs');
app.set('views','views');

var adminRouter = express.Router('/admin');


adminRouter
.route('/')
.get((req,res) => {
    res.status(200)
    if(req.session.loggedin) {
        var haircuts = ''
        pool.query(`SELECT * FROM haircuts`, (err,data) => {
            if (data) {
                for (var i = 0;i<data.length;i++) {
                    haircuts+=`<tr>
                            <th class="haircutId" scope="row">${data[i].idhaircuts}</th>
                            <td class="haircutName">${data[i].title}</td>
                            <td class="haircutPrice">${data[i].price}</td>
                            <td class="haircutImage">${data[i].route_picture}</td>
                            <td class='updateButton' id='updateButton${i}'><button data-modal-haircut-change class="menu_item_btn">Редактировать</button></td>
                            <td><button class="menu_item_btn" name='deleteHaircut'>Удалить</button></td>
                            </tr>`
                }
            }
            var signs = ''
            pool.query('SELECT * FROM sign_haircut', (err,data) => {
                if (data) {
                    for (var i = 0;i<data.length;i++) {

                        signs+=`<tr>
                                    <th scope="row"class='sign_id'>${data[i].id}</th>
                                    <td class='sign_name'>${data[i].name}</td>
                                    <td class='sign_number'>${data[i].number}</td>
                                    <td class='sign_haircutID'>${data[i].haircutID}</td>
                                    <td class='sign_stylistID'>${data[i].stylistID}</td>
                                    <td class='sign_date'>${data[i].sign_date}</td>
                                    <td class='sign_time'>${timeCollection[`${data[i].sign_time}`]}</td>
                                    <td><button class="menu_item_btn" name='deleteSign'>Удалить</button></td>
                                </tr>`
                    }
                } 
                var applies = ''
                pool.query('SELECT * FROM job_apply', (err,data) => {
                    if (data) {
                        for (var i = 0; i <data.length;i++) {
                            applies+=`  <tr>
                                            <th scope="row" class='apply_id'>${data[i].id}</th>
                                            <td>${data[i].name}</td>
                                            <td>${data[i].phone_number}</td>
                                            <td><button class="menu_item_btn" name='deleteApply'>Обработано</button></td>
                                        </tr>`
                        }
                    }
                    var calls = ''
                    pool.query('SELECT * FROM order_call', (err,data) => {
                        if (data) {
                            for (var i = 0; i<data.length;i++) {
                                calls+=`<tr>
                                        <th scope="row" class='calls_id'>${data[i].id}</th>
                                        <td>${data[i].name}</td>
                                        <td>${data[i].phone_number}</td>
                                        <td><button class="menu_item_btn" name='deleteCall'>Обработано</button></td>              
                                    </tr>`
                            }
                        }
                        res.render('admin', {
                            haircuts: haircuts,
                            signs: signs,
                            applies: applies,
                            calls: calls
                        })
                    })
                })
            })
        })
    }
    else {
        res.redirect('auth')
    }
})
adminRouter
.route('/update_haircut') 
.post(urlencodedParser, (req,res)=>{
    var imageRoute = ''
    if(req.files) {
        fs.unlinkSync(`public${req.body.haircutChangeImage}`)
        req.files.photo.mv('public/image/haircut/'+req.files.photo.name);
        imageRoute = `/image/haircut/${req.files.photo.name}`
    }
    else {
        imageRoute = req.body.haircutChangeImage;
    }
    pool.query(`UPDATE haircuts SET route_picture='${imageRoute}', title='${req.body.name}',price=${req.body.haircutChangePrice} WHERE route_picture='${req.body.haircutChangeImage}'`, (err,data) => {
        res.redirect('/admin')
    })
})
adminRouter
.route('/add_haircut')
.post(urlencodedParser, (req,res) => {
    
    pool.query(`INSERT INTO haircuts(title,price,route_picture) VALUES ('${req.body.name}',${req.body.haircutdownloadPrice},'/image/haircut/${req.files.photo.name}')`, (err,data) => {
        req.files.photo.mv('public/image/haircut/'+req.files.photo.name);
        res.redirect('/admin')
    })
})
adminRouter
.route('/delete_haircut')
.post(urlencodedParser, (req,res) => {
    pool.query(`SELECT * FROM haircuts WHERE idhaircuts='${req.body.id}'`, (err,data) => {
        if(err) throw err;
        else {
            fs.unlinkSync(`public${data[0].route_picture}`)
            pool.query(`DELETE FROM haircuts WHERE idhaircuts=${req.body.id}`, (err,data) => { 
            })
            pool.query('ALTER TABLE haircuts AUTO_INCREMENT = 1')
            res.json('ok')
        }
    })
})
adminRouter
.route('/delete_sign')
.post(urlencodedParser, (req,res) => {
    pool.query(`DELETE FROM sign_haircut WHERE id='${req.body.id}'`, (err,data) => {
        console.log(data)
        res.json('ok')
    })
    pool.query('ALTER TABLE sign_haircut AUTO_INCREMENT = 1')
})
adminRouter
.route('/delete_apply')
.post(urlencodedParser, (req,res) => {
    pool.query(`DELETE FROM job_apply WHERE id='${req.body.id}'`, (err,data) => {
        console.log(data)
        res.json('ok')
    })
    pool.query('ALTER TABLE job_apply AUTO_INCREMENT = 1')
})
adminRouter
    .route('/delete_call')
    .post(urlencodedParser, (req,res) => {
        pool.query(`DELETE FROM order_call WHERE id='${req.body.id}'`, (err,data) => {
            console.log(data)
            res.json('ok')
        })
        pool.query('ALTER TABLE order_call AUTO_INCREMENT = 1')
    })
app.route('/auth')
    .get((req,res) => {
        res.status = 200;
        res.render('auth')
    })
    .post(urlencodedParser, (req,res)=>{
        auth(crypto,pool,req,res)
    })

app.use('/admin',adminRouter)

app.get('/', (req,res)=>{
    pool.query('SELECT * FROM haircuts', (err,data) => {
        var dataToRender = ''
        if (data) {
            for (var i = 0; i< data.length;i++) {
                dataToRender+=`<div class="col-md-4">
                                    <a data-modal-haircut> 
                                        <img src="${data[i].route_picture}" alt="hair">
                                        <div class="haircut_type" id="${data[i].idhaircuts}">
                                            <div class="haircut_type_title" >${data[i].title}</div>
                                            <div class="haircut_type_price">Цена: ${data[i].price}р</div>
                                        </div>
                                    </a>
                                </div>`
            }
        }
        res.status(200)
        res.render("index.hbs", {
            haircuts: dataToRender
        })
    })
})

app.route('/job_apply')
    .get((req,res)=>{
        res.status(200)
        res.render('job_apply', {
            //параметры для главной страницы(прически)
        })
    })
    .post(urlencodedParser, (req,res)=> {
        if (!req.body) return res.sendStatus(400);
        pool.query(`INSERT INTO job_apply(name, phone_number) VALUES ('${req.body.name}','${req.body.phone_number}')`, (err,data)=>{
            console.log(data)
        })
        res.redirect('/')
    });

app.route('/order_call')
    .get((req,res)=>{
        res.status(200)
        res.render('order_call', {
            //параметры для главной страницы(прически)
        })
    })
    .post(urlencodedParser, (req,res)=> {
        if (!req.body) return res.sendStatus(400);
        pool.query(`INSERT INTO order_call(name, phone_number, state) VALUES ('${req.body.name}','${req.body.phone_number}','not_processed')`, (err,data)=>{
            console.log(data)
        })
        res.redirect('/');
    });
app.route('/sign_haircut')
    .get((req,res)=>{
        res.status(200)
        res.render('sign_haircut', {
            //параметры для главной страницы(прически)
        })
    })
    .post(urlencodedParser, (req,res)=> {
        if (!req.body) return res.sendStatus(400);
        pool.query(`INSERT INTO sign_haircut(name,number,haircutID,stylistID,sign_date,sign_time) 
        VALUES ('${req.body.name}','${req.body.phone_number}',${req.body.haircutID}, ${req.body.stylistID[0]},'${req.body.date_seance}',${req.body.stylistID[1]})`, (err,data)=>{
            console.log(data)
            pool.query(`UPDATE session_time SET session_interval_${req.body.stylistID[1]}=0 WHERE date='${req.body.date_seance}'`)
        })
        res.redirect('/');
    });
app.route('/date')
    .get((req,res)=>{
        res.status(200)
    })
    .post(urlencodedParser, (req,res)=> {
        if (!req.body) return res.sendStatus(400);
        var date = req.body.date;
        pool.query(`SELECT * FROM session_time WHERE date='${date}'`, (err,data) => {
            if (err) {
                res.send('err')
            }
            else {
                var dataToRes = ''
                if (data) {
                    dataToRes = '<option value="" disabled selected>Выберите свободное время</option>'
                    for(var i = 0; i < 6; i++ ) {
                        if (data[0][`session_interval_${i+1}`] === 1) {
                            dataToRes += `<option value="${i+1}"> ${timeCollection[`${i+1}`]}</option>`;
                        }
                    }
                    
                }
                res.json(dataToRes)
            }
        })
    });


//запуск сервера
app.listen(PORT,'185.255.134.30', () => {
    console.log(`Server started on ${PORT}...`);
});