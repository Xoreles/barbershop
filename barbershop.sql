-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 20 2021 г., 18:09
-- Версия сервера: 8.0.27
-- Версия PHP: 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `barbershop`
--

-- --------------------------------------------------------

--
-- Структура таблицы `haircuts`
--

DROP TABLE IF EXISTS `haircuts`;
CREATE TABLE IF NOT EXISTS `haircuts` (
  `idhaircuts` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `price` int DEFAULT NULL,
  `route_picture` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idhaircuts`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `job_apply`
--

DROP TABLE IF EXISTS `job_apply`;
CREATE TABLE IF NOT EXISTS `job_apply` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(40) DEFAULT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `order_call`
--

DROP TABLE IF EXISTS `order_call`;
CREATE TABLE IF NOT EXISTS `order_call` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `state` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `session_time`
--

DROP TABLE IF EXISTS `session_time`;
CREATE TABLE IF NOT EXISTS `session_time` (
  `date` date NOT NULL,
  `session_interval_1` int DEFAULT NULL,
  `session_interval_2` int DEFAULT NULL,
  `session_interval_3` int DEFAULT NULL,
  `session_interval_4` int DEFAULT NULL,
  `session_interval_5` int DEFAULT NULL,
  `session_interval_6` int DEFAULT NULL,
  PRIMARY KEY (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `session_time`
--

INSERT INTO `session_time` (`date`, `session_interval_1`, `session_interval_2`, `session_interval_3`, `session_interval_4`, `session_interval_5`, `session_interval_6`) VALUES
('2021-12-21', 1, 0, 1, 1, 1, 1),
('2021-12-22', 1, 1, 1, 0, 1, 1),
('2021-12-23', 1, 0, 0, 1, 0, 1),
('2021-12-24', 0, 0, 0, 0, 0, 1),
('2021-12-25', 1, 1, 1, 0, 0, 1),
('2021-12-26', 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `sign_haircut`
--

DROP TABLE IF EXISTS `sign_haircut`;
CREATE TABLE IF NOT EXISTS `sign_haircut` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(40) DEFAULT NULL,
  `number` varchar(20) DEFAULT NULL,
  `haircutID` int DEFAULT NULL,
  `stylistID` int DEFAULT NULL,
  `sign_date` varchar(40) DEFAULT NULL,
  `sign_time` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `userid` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `role` varchar(45) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`userid`, `name`, `role`, `password`) VALUES
(2, 'admin', 'admin', '69c6fda19329b530a43354615c573bf640de9d59a814d8cf3a9760c2e5c614d8');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
