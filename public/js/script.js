"use strict";
window.addEventListener('DOMContentLoaded', () => {
    const modalTriggerOrder = document.querySelectorAll('[data-modal-order]'),
          modalOrder = document.querySelector('.modal-order'),
          modalCloseBtnOrder = document.querySelector('[data-close-order]'),

          modalTriggerJob = document.querySelectorAll('[data-modal-job]'),
          modalJob = document.querySelector('.modal-job'),
          modalCloseBtnJob = document.querySelector('[data-close-job]'),

          modalTriggerRecord = document.querySelectorAll('[data-modal-record]'),
          modalRecord = document.querySelector('.modal-record'),
          modalCloseBtnRecord = document.querySelector('[data-close-record]'),


          modalTriggerHaircut = document.querySelectorAll('[data-modal-haircut]'),
          modalHaircut = document.querySelector('.modal-haircut'),
          modalCloseBtnHaircut = document.querySelector('[data-close-haircut]'),

          haircutname = document.getElementById("haircutname"),
          haircutid = document.getElementById("haircutid");


//HAIRCUT
    function openModalHaircut(id, name){
        modalHaircut.classList.add('show');
        modalHaircut.classList.remove('hide');
        haircutid.value = id;
        document.body.style.overflow = 'hidden';
        haircutname.innerHTML = "Прическа: " + name;

    }
    modalTriggerHaircut.forEach(btn =>{
        btn.addEventListener("click", e=>{
            var id = e.target.id;
            var name =  '';
            if (e.target.className === ".haircut_type_title" || ".haircut_type_price") {
                id = e.target.parentNode.id;
                name = e.target.parentNode.querySelector('.haircut_type_title').textContent;
            }
            else {
                name = e.target.childNodes[1].textContent;
            }
            openModalHaircut(id, name);
        });
    });

    modalHaircut.addEventListener('click', e => {
        if (e.target === modalHaircut) {
            closeModalHaircut();
        }
    });

    document.addEventListener('keydown', (e) => {
        if (e.code === "Escape" && modalHaircut.classList.contains('show')){
            closeModalHaircut();
        }
    });

    function closeModalHaircut(){
        modalHaircut.classList.add('hide');
        modalHaircut.classList.remove('show');
        document.body.style.overflow = '';
    }
    
    modalCloseBtnHaircut.addEventListener("click", closeModalHaircut);
//ORDER
    function openModalOrder(){
        modalOrder.classList.add('show');
        modalOrder.classList.remove('hide');
        document.body.style.overflow = 'hidden';
    }

    modalTriggerOrder.forEach(btn =>{
        btn.addEventListener("click", openModalOrder);
    });

    function closeModalOrder(){
        modalOrder.classList.add('hide');
        modalOrder.classList.remove('show');
        document.body.style.overflow = '';
    }

    modalCloseBtnOrder.addEventListener("click", closeModalOrder);

    modalOrder.addEventListener('click', (e) => {
        if (e.target === modalOrder) {
            closeModalOrder();
        }
    });

    document.addEventListener('keydown', (e) => {
        if (e.code === "Escape" && modalOrder.classList.contains('show')){
            closeModalOrder();
        }
    });


//JOB
    modalCloseBtnJob.addEventListener("click", closeModalJob);

    function openModalJob(){
        modalJob.classList.add('show');
        modalJob.classList.remove('hide');
        document.body.style.overflow = 'hidden';
    }

    modalTriggerJob.forEach(btn =>{
        btn.addEventListener("click", openModalJob);
    });

    modalJob.addEventListener('click', (e) => {
        if (e.target === modalJob) {
            closeModalJob();
        }
    });

    document.addEventListener('keydown', (e) => {
        if (e.code === "Escape" && modalJob.classList.contains('show')){
            closeModalJob();
        }
    });

    function closeModalJob(){
        modalJob.classList.add('hide');
        modalJob.classList.remove('show');
        document.body.style.overflow = '';
    }

//RECORD
    modalCloseBtnRecord.addEventListener("click", closeModalRecord);

    function openModalRecord(){
        modalRecord.classList.add('show');
        modalRecord.classList.remove('hide');
        document.body.style.overflow = 'hidden';
    }

    modalTriggerRecord.forEach(btn =>{
        btn.addEventListener("click", openModalRecord);
    });

    modalRecord.addEventListener('click', (e) => {
        if (e.target === modalRecord) {
            closeModalRecord();
        }
    });

    document.addEventListener('keydown', (e) => {
        if (e.code === "Escape" && modalRecord.classList.contains('show')){
            closeModalRecord();
        }
    });

    function closeModalRecord(){
        modalRecord.classList.add('hide');
        modalRecord.classList.remove('show');
        document.body.style.overflow = '';
    }

    const inputDate = document.querySelector('#date-record');
    const timeArray = document.querySelector('#listTime');
    inputDate.oninput = function() {
        $.ajax({ 
            type: 'POST', 
            url: '/date', 
            data: {
                    date: inputDate.value
                }, 
            dataType: 'json',
            success: function (res) {
                timeArray.innerHTML = res;
            },
        });
    };

});