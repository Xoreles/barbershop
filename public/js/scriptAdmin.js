const modalTriggerHaircutDownload = document.querySelectorAll('[data-modal-haircut-download]'),
    modalHaircutDownload = document.querySelector('.modal-haircut-download'),
    modalCloseBtnHaircutDownload = document.querySelector('[data-close-haircut-download]'),
    modalTriggerHaircutChange = document.querySelectorAll('[data-modal-haircut-change]'),
    modalHaircutChange = document.querySelector('.modal-haircut-change'),
    modalCloseBtnHaircutChange = document.querySelector('[data-close-haircut-change]');


function openModalHaircutChange() {
    modalHaircutChange.classList.add('show');
    modalHaircutChange.classList.remove('hide');
    document.body.style.overflow = 'hidden';
}

modalTriggerHaircutChange.forEach(btn => {
    btn.addEventListener("click", () => {
        openModalHaircutChange();
    });
});

modalHaircutChange.addEventListener('click', (e) => {
    if (e.target === modalHaircutChange) {
        closeModalHaircutChange();
    }
});

document.addEventListener('keydown', (e) => {
    if (e.code === "Escape" && modalHaircutChange.classList.contains('show')) {
        closeModalHaircutChange();
    }
});

function closeModalHaircutChange() {
    modalHaircutChange.classList.add('hide');
    modalHaircutChange.classList.remove('show');
    document.body.style.overflow = '';
}

modalCloseBtnHaircutChange.addEventListener("click", closeModalHaircutChange);


function openModalHaircutDownload() {
    modalHaircutDownload.classList.add('show');
    modalHaircutDownload.classList.remove('hide');
    document.body.style.overflow = 'hidden';
}

modalTriggerHaircutDownload.forEach(btn => {
    btn.addEventListener("click", e => {
        openModalHaircutDownload();
    });
});

modalHaircutDownload.addEventListener('click', (e) => {
    if (e.target === modalHaircutDownload) {
        closeModalHaircutDownload();
    }
});

document.addEventListener('keydown', (e) => {
    if (e.code === "Escape" && modalHaircutDownload.classList.contains('show')) {
        closeModalHaircutDownload();
    }
});

function closeModalHaircutDownload() {
    modalHaircutDownload.classList.add('hide');
    modalHaircutDownload.classList.remove('show');
    document.body.style.overflow = '';
}

modalCloseBtnHaircutDownload.addEventListener("click", closeModalHaircutDownload);

//Подвязка форм к существующим записям на странице
const trUpdateButton = document.querySelectorAll('.updateButton');

const updateButton = [],
      updateForm = document.querySelector('.modal-haircut-change'),
      updateFormName = updateForm.querySelector('#nameUpdate'),
      updateFormPrice = updateForm.querySelector('#priceUpdate'),
      updateFormPicture = updateForm.querySelector('#imageUpdate'),
      updateFormId = updateForm.querySelector('#idUpdate');
for (let i = 0; i < trUpdateButton.length; i++) {
    updateButton[i] = trUpdateButton[i].firstChild;
}
for (let j = 0; j < updateButton.length; j++) {
    updateButton[j].addEventListener('click', () => {
        var pElem = updateButton[j].parentElement.parentElement,
            pElemId = pElem.querySelector('.haircutId'),
            pElemName = pElem.querySelector('.haircutName'),
            pElemPrice = pElem.querySelector('.haircutPrice'),
            pElemImage = pElem.querySelector('.haircutImage');
        updateFormName.value = pElemName.textContent;
        updateFormId.value = pElemId.textContent;
        updateFormPrice.value = pElemPrice.textContent;
        updateFormPicture.value = pElemImage.textContent;
    });
}
//Кнопки удаления для всех полей в cms
const deleteHaircut = document.getElementsByName('deleteHaircut');
deleteHaircut.forEach(btn=>{
    btn.addEventListener('click', () => {
        var id = btn.parentElement.parentElement.querySelector('.haircutId').textContent
        $.ajax({ 
            type: 'POST', 
            url: '/admin/delete_haircut', 
            data: {
                    id: id
                }, 
            dataType: 'json',
            success: function (res) {
                location.reload()
            }
        });
    })
})
const deleteSign = document.getElementsByName('deleteSign');
deleteSign.forEach(btn=>{
    btn.addEventListener('click', ()=>{
        var id = btn.parentElement.parentElement.querySelector('.sign_id').textContent
        $.ajax({ 
            type: 'POST', 
            url: '/admin/delete_sign', 
            data: {
                    id: id
                }, 
            dataType: 'json',
            success: function (res) {
                location.reload()
            }
        });
    })
})
const deleteApply = document.getElementsByName('deleteApply');
deleteApply.forEach(btn=>{
    btn.addEventListener('click', ()=>{
        var id = btn.parentElement.parentElement.querySelector('.apply_id').textContent
        $.ajax({ 
            type: 'POST', 
            url: '/admin/delete_apply', 
            data: {
                    id: id
                }, 
            dataType: 'json',
            success: function (res) {
                location.reload()
            }
        });
    })
})
const deleteCall = document.getElementsByName('deleteCall');
deleteCall.forEach(btn=>{
    btn.addEventListener('click', ()=>{
        var id = btn.parentElement.parentElement.querySelector('.calls_id').textContent
        $.ajax({ 
            type: 'POST', 
            url: '/admin/delete_call', 
            data: {
                    id: id
                }, 
            dataType: 'json',
            success: function (res) {
                location.reload()
            }
        });
    })
})